import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const home = functions.https.onRequest((_req, res) => {
  res.send("Hello from Firebase!");
});

export const bot1 = functions.https.onRequest((_req, res) => {
  res.send("Hello from Bot1 !");
});

export const bot2 = functions.https.onRequest((_req, res) => {
  res.send("Hello from Bot2 !");
});

